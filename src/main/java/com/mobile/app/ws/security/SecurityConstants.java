package com.mobile.app.ws.security;

import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.security.Key;

public class SecurityConstants {
    public static final long EXPIRATION_TIME = 864000000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/users";
//    public static final String TOKEN_SECRET = "fjlj4lhkfhkd08kcssfjdslfjlj4l2j3ljldsjlgjlgjfdljlxjlj4ljljlnloewroz49832948329499cdknklmdlamlcmlsdnvlxnvknl";
    public static final Key TOKEN_SECRET = Keys.secretKeyFor(SignatureAlgorithm.HS256);

}
